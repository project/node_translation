<?php
// $Id

/**
 * @file
 * Admin page callback file for the node_translation module.
 */

//////////////////////////////////////////////////////////////////////////////
// Node Translation Admin Settings

/**
 * Settings form display.
 */
function node_translation_admin_settings() {
  $translatable_languages = call_user_func('translation_framework_languages');

  $form['node_translation'] = array('#type' => 'fieldset', '#title' => t('Node Translation Settings'), '#collapsible' => TRUE, '#collapsed' => FALSE);
  $form['node_translation']['node_translation_source_language_default'] = array(
    '#type'          => 'select',
    '#title'         => t('Source translation language'),
    '#default_value' => NODE_TRANSLATION_SOURCE_LANGUAGE_DEFAULT,
    '#options'       => $translatable_languages,
    '#description'   => t('Language to utilize if the translation is unknown'),
  );
  $form['node_translation']['node_translation_remove_links'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove Translation Module links'),
    '#default_value' => NODE_TRANSLATION_REMOVE_LINKS,
    '#description'   => t('Removes the translation module links so we only have the node translation module link which provides the same functionality'),
  );
  return system_settings_form($form);
}
